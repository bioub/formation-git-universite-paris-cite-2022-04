# Formation Git Université Paris Cité des 21 et 22 avril 2022

Formation Git pour *Université Paris Cité* du 21 et 22 avril 2022

## Commandes de base

- git init
- git config
- git add
- git status
- git commit

## Commandes de branches

- git branch
- git checkout
- git switch
- git merge

## Le répertoire .git

- objects (commit, tag, index, fichiers...)
- refs
- HEAD
- config
- info/exclude

## Workflows

- Centralized
- gitflow
- Github flow
- Gitlab flow
